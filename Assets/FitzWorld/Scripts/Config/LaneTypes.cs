﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class LanesContainer
{
    public List<Lane> laneList = new List<Lane>();
}

[Serializable]
public class Lane
{
    public int laneIndex = 0;
    public bool isMainPlayer = false;
    public string name = "";
    public string parentId = "";
    public string playerType = "";
    public string playerConfig = ""; 
}
