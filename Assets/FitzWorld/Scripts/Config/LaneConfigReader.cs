﻿using UnityEngine;
using System.Collections.Generic;

public class LaneConfigReader
{

    private List<Lane> laneList = null;

    public LaneConfigReader(string givenConfig)
    {
        string json = System.IO.File.ReadAllText(givenConfig);
        LanesContainer lanesContainer = JsonUtility.FromJson<LanesContainer>(json);
        laneList = lanesContainer.laneList;

        //TODO: What to do 
        //throw exception if config does not exist
        //throw new System.ArgumentException("Config does not exist!");
    }

    public bool hasLane(string name)
    {
        foreach (Lane lane in laneList)
        {
            if (lane.name == name)
            {
                return true;
            }
        }
        return false;
    }

    public Lane getLane(string name)
    {
        foreach (Lane lane in laneList)
        {
            if(lane.name == name)
            {
                return lane;
            }
        }

        //Throw exception if the given lane is not found
        throw new System.ArgumentOutOfRangeException("Name not found!");
    }
}
