﻿using UnityEngine;
using ProtoBuf;
using EasyErgsocket;
using NetMQ;
using NetMQ.Sockets;
using System.Collections.Generic;
using System.Linq;

public class ErgManager : MonoBehaviour
{
    NetMQContext context = null;
    SubscriberSocket subSocket = null;
    
    //String: Id of the Erg
    //GameObject: The boat
    IDictionary<string, GameObject> boats = new Dictionary<string, GameObject>();
    
    TrackManager trackManager = null;
    StatDisplayManager statDisplayManager = null;
    CameraManager cameraManager = null;
    LaneConfigReader laneConfig = null;

    public GameObject playerBoatType;
    public GameObject otherBoatType;
    public float laneDistance = 4.0f;
    public string address = "tcp://127.0.0.1:21744";
    public string playerConfig = "./worldConfig.json";
    
    void Start ()
    {
        trackManager = GetComponent<TrackManager>();
        statDisplayManager = GetComponent<StatDisplayManager>();
        cameraManager = GetComponent<CameraManager>();

        try
        {
            laneConfig = new LaneConfigReader(playerConfig);
        }
        catch (System.ArgumentException)
        {
            Debug.Log("Cannot load LaneConfig " + playerConfig);
            Application.Quit();
        }

        //if there is no otherBoatType set just use the playerBoatType... if this isn't set either we're screwed anyways...
        if(otherBoatType == null)
        {
            otherBoatType = playerBoatType;
        }

        Debug.Log("Starting up NetMQ interface on " + address);
        context = NetMQContext.Create();
        subSocket = context.CreateSubscriberSocket();
        subSocket.Connect(address);
        subSocket.Subscribe("EasyErgsocket");
    }
    
    void Update()
    {
        //Try to receive data and apply it to the boats
        IList<Erg> receivedBoats = ReceiveBoats();
        foreach (EasyErgsocket.Erg erg in receivedBoats)
        {
            //do not do anything if there's no lane for the given erg
            if (!laneConfig.hasLane(erg.name))
            {
                continue;
            }

            //update everything
            UpdateErg(erg);

            //if the given erg is main player we need to update everything around the boat
            Lane ergLane = laneConfig.getLane(erg.name);
            if (ergLane.isMainPlayer)
            {
                if (statDisplayManager != null)
                {
                    statDisplayManager.UpdatePosition((float)erg.distance);
                }
            }
        }
    }

    private IList<Erg> ReceiveBoats()
    {
        //try to receive something from the network... return all the ergs we get
        var message = new NetMQMessage();
        IList<EasyErgsocket.Erg> receivedBoats = new List<EasyErgsocket.Erg>();
        while (subSocket.TryReceiveMultipartMessage(System.TimeSpan.Zero, ref message))
        {
            foreach (var frame in message.Skip(1)) //the first frame is always just the envelope/topic... let's ignore it by using Linq
            {
                byte[] rawMessage = frame.Buffer;
                using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(rawMessage))
                {
                    var givenErg = Serializer.Deserialize<EasyErgsocket.Erg>(memoryStream);
                    receivedBoats.Add(givenErg);
                }
            }
        }

        return receivedBoats;
    }

    private void UpdateErg(Erg givenErg)
    {
        //Check if the current boat should be created, create it if needed
        if (!boats.ContainsKey(givenErg.ergId)) //has the boat been created before?
        {
            CreateBoat(givenErg);
        }

        //update the boat
        if(boats.ContainsKey(givenErg.ergId)) //do not try to update non-existing boats
        {
            Boat boat = boats[givenErg.ergId].GetComponent<Boat>();
            boat.UpdatePosition((float)givenErg.distance, (float)givenErg.exerciseTime);

            Lane ergLane = laneConfig.getLane(givenErg.name);
            if (ergLane.isMainPlayer)
            {
                if (trackManager != null)
                {
                    trackManager.SetDistance((float)givenErg.distance);
                }
            }
        }
    }

    private void CreateBoat(Erg givenErg)
    {
        //get the lane for the given erg
        Lane ergLane = laneConfig.getLane(givenErg.name);

        //create the boat
        Vector3 newPos = new Vector3();
        newPos.z = laneDistance * ergLane.laneIndex;
        Quaternion newRot = new Quaternion();
        if(ergLane.isMainPlayer)
        {
            boats[givenErg.ergId] = Instantiate(playerBoatType, newPos, newRot) as GameObject;
            if (cameraManager != null)
            {
                cameraManager.SetParent(boats[givenErg.ergId].transform);
            }
        }
        else
        {
            boats[givenErg.ergId] = Instantiate(otherBoatType, newPos, newRot) as GameObject;
        }
        
        //update the boats name etc
        boats[givenErg.ergId].GetComponent<Boat>().BoatName = givenErg.name;
        Debug.Log("Created new Boat " + givenErg.ergId + " with name " + givenErg.name);
    }

    void OnApplicationQuit()
    {
        Debug.Log("Shutting down...");
        subSocket.Close();
        context.Terminate();
        Debug.Log("Done...");
    }
}
